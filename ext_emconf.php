<?php
$EM_CONF[$_EXTKEY] = [
  'title' => 'Customer Site',
  'description' => 'Site package example',
  'category' => 'templates',
  'author' => 'Ricahrd Schaufler',
  'author_email' => 'mail@richardschaufler.de',
  'author_company' => 'IT-Consulting Richard Schaufler',
  'shy' => '',
  'priority' => '',
  'module' => '',
  'state' => 'stable',
  'internal' => '',
  'uploadfolder' => '0',
  'createDirs' => '',
  'modify_tables' => '',
  'clearCacheOnLoad' => 0,
  'lockType' => '',
  'version' => '1.0.0',
  'constraints' =>
  [
    'depends' =>
    [
      'typo3' => '9.5.0-9.5.99',
    ],
    'conflicts' =>
    [
    ],
    'suggests' =>
    [
    ],
  ],
];
