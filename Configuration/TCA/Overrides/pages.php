<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function($extKey = 'customer_site')
    {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
            $extKey,
            'Configuration/TSconfig/Page/page.tsconfig',
            'Customer Site'
        );
    }
);
